-- CREATE DATABASE WBContent;

CREATE TABLE public.cities (
    ind INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    nam VARCHAR(100) NOT NULL,
    country CHAR(2) NOT NULL, 
    lat FLOAT NOT NULL,
    lon FLOAT NOT NULL
);

CREATE TABLE public.weathers (
    ind INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    city_ind INT NOT NULL,
    dte TIMESTAMP NOT NULL,
    daily_tmp FLOAT NOT NULL,
    rem JSONB,
    CONSTRAINT fk_city
    FOREIGN KEY(city_ind)
    REFERENCES public.cities(ind)
);


-- SELECT * FROM weathers;
-- UPDATE weathers
-- SET daily_tmp = 14.10
-- WHERE city_ind = 14;
-- SELECT * FROM weathers;


-- DO
-- $do$
--     BEGIN
--     IF EXISTS 
--         (SELECT * FROM weathers WHERE ind = 85)
--         THEN
--             UPDATE weathers
--             SET 
--                 daily_tmp = 22,
--                 rem = NULL
--             WHERE ind = 85;
--         ELSE
--             INSERT INTO weathers 
--             ("city_ind", "dte", "daily_tmp")
--             VALUES (14, CURRENT_TIMESTAMP, 27);
--         END IF;
--     END
-- $do$

--  ELSE 

--  update #B
--  set code = 123
--  from #B


-- SELECT * FROM cities;
-- SELECT * FROM weathers;

-- SELECT * FROM weathers WHERE city_ind = 10;

-- INSERT INTO cities 
-- ("nam", "country", "lat", "lon")
-- VALUES 
-- ('Moscow', 'RU', 55.7504461, 37.6174943),
-- ('Moscow', 'RU', 55.7504461, 37.6174943),
-- ('Moscow', 'RU', 55.7504461, 37.6174943);

-- INSERT INTO weathers 
-- ("city_ind", "dte", "daily_tmp")
-- VALUES (14, CURRENT_TIMESTAMP, 27);


-- DELETE FROM public.cities;
-- DELETE FROM public.weathers;

-- DELETE FROM public.weathers
-- WHERE ind = 32 OR ind = 34

-- DROP TABLE public.weathers;
-- DROP TABLE public.cities;
