package models

// City is a structure that represents a city
type City struct {
	ID      int     // required for database
	Name    string  // city full name
	Country string  // country code. For example 'GB'
	Lat     float64 // coordinate
	Lon     float64 // coordinate
}
