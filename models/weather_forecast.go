package models

import "time"

// Forecast is a structure that represents a weather forecast
type Forecast struct {
	ID            int                    // weather forecast id
	CityID        int                    // weather forecast city
	Temperature   float64                // average temperature for весь доступный будущий период
	Date          time.Time              // weather forecast timestamp
	RemainingJSON map[string]interface{} // additional information
}
