package models

import "time"

type ShortForecast struct {
	City     City
	AvgTemp  float64
	DateList []time.Time
}
