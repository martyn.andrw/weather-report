package server

import (
	"log"
	"net/http"
)

type server struct {
	mux *http.ServeMux
}

func NewServer() *server {
	return &server{
		mux: http.NewServeMux(),
	}
}

func (s *server) HandleFunc(pattern string, handler func(w http.ResponseWriter, r *http.Request)) {
	log.Printf("added pat:%s to mux\n", pattern)
	s.mux.HandleFunc(pattern, handler)
}

func (s *server) Run(host, port string) error {
	log.Printf("started %s:%s", host, port)
	return http.ListenAndServe(host+":"+port, s.mux)
}
