package service

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"sort"
	"sync"

	"gitlab.com/martyn.andrw/weather-report/models"
)

type CityS struct {
	repository RepositoryCity
	cfg        ConfigCity

	cities []*models.City
	mutex  *sync.RWMutex
}

type ConfigCity struct {
	GeoReverseLink string
	GeoDirectLink  string
	Apikey         string
}

type RepositoryCity interface {
	AddNewCity(city *models.City) error
	AddNewCities(cities []*models.City) error
	GetCityByName(name string) (*models.City, error)
	GetCities() ([]*models.City, error)
}

func NewCityService(cityRepository RepositoryCity, cfg ConfigCity) (*CityS, error) {
	cities, err := cityRepository.GetCities()
	if err != nil {
		return nil, err
	}

	indb := make([]*models.City, 0, 20)

	for len(cities) < 20 {
		lat, lon := float64(rand.Intn(180)-90)+rand.Float64(), float64(rand.Intn(360)-180)+rand.Float64()
		add, err := getCityGeo(cfg.GeoReverseLink, cfg.Apikey, lat, lon)
		if err != nil {
			return nil, err
		}

		// todo: fix originality
		for len(cities) < 20 && len(add) > 0 {
			cities = append(cities, add[len(add)-1])
			indb = append(indb, add[len(add)-1])
			add = add[:len(add)-1]
		}
	}

	if len(indb) > 0 {
		err = cityRepository.AddNewCities(indb)
		if err != nil {
			return nil, err
		}
	}

	sort.Slice(cities, func(i, j int) bool {
		return cities[i].Name < cities[j].Name
	})

	return &CityS{
		repository: cityRepository,
		cfg:        cfg,
		cities:     cities,
		mutex:      new(sync.RWMutex),
	}, nil
}

func (c *CityS) GetOrdered() []*models.City {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	res := make([]*models.City, 0, 20)
	for _, c := range c.cities {
		res = append(res, copyCity(c))
	}
	return res
}

func (c *CityS) GetCityByName(name string) (*models.City, error) {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	// todo: fix situation, when 2 or more cities have same name
	for _, city := range c.cities {
		if city.Name == name {
			return copyCity(city), nil
		}
	}
	return nil, errCityNotFound
}

type cityResponce struct {
	Name    string  `json:"name"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Country string  `json:"country"`
}

func (c cityResponce) convertToModel() *models.City {
	return &models.City{
		Name:    c.Name,
		Lat:     c.Lat,
		Lon:     c.Lon,
		Country: c.Country,
	}
}

func getCityGeo(link, apikey string, lat, lon float64) ([]*models.City, error) {
	url := fmt.Sprintf("%s?apikey=%s&lat=%v&lon=%v&limit=20", link, apikey, lat, lon)
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var resCities []cityResponce
	err = json.Unmarshal(b, &resCities)
	if err != nil {
		return nil, err
	}

	result := make([]*models.City, 0, len(resCities))
	for _, city := range resCities {
		result = append(result, city.convertToModel())
	}
	return result, nil
}

func copyCity(city *models.City) *models.City {
	return &models.City{
		ID:      city.ID,
		Name:    city.Name,
		Country: city.Country,
		Lat:     city.Lat,
		Lon:     city.Lon,
	}
}
