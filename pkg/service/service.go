package service

import (
	"log"
	"sync"
	"time"

	"gitlab.com/martyn.andrw/weather-report/models"
)

type CityService interface {
	GetOrdered() []*models.City
	GetCityByName(name string) (*models.City, error)
}

type ForecastService interface {
	GetShortForecastForCity(city *models.City) (*models.ShortForecast, error)
	GetFullForecast(city *models.City, date time.Time) (*models.Forecast, error)
	UpdateForCity(city *models.City) error
}

type Service struct {
	cities    CityService
	forecasts ForecastService
}

func NewService(citiesService CityService, forecastService ForecastService) *Service {
	return &Service{
		cities:    citiesService,
		forecasts: forecastService,
	}
}

func (s *Service) GetOrderedCities() []*models.City {
	return s.cities.GetOrdered()
}

func (s *Service) GetCityByName(name string) (*models.City, error) {
	return s.cities.GetCityByName(name)
}

func (s *Service) GetShortForecastForCity(city *models.City) (*models.ShortForecast, error) {
	return s.forecasts.GetShortForecastForCity(city)
}

func (s *Service) GetFullForecast(city *models.City, date time.Time) (*models.Forecast, error) {
	return s.forecasts.GetFullForecast(city, date)
}

func (s *Service) UpdateEveryMinute() {
	s.UpdateForecasts()
	ticker := time.NewTicker(time.Minute)

	for range ticker.C {
		s.UpdateForecasts()
	}
}

func (s *Service) UpdateForecasts() {
	log.Println("started update forecasts")
	wg := sync.WaitGroup{}

	for _, city := range s.GetOrderedCities() {
		wg.Add(1)

		go func(wg *sync.WaitGroup, city *models.City) {
			err := s.forecasts.UpdateForCity(city)
			if err != nil {
				log.Println(err)
			}
			wg.Done()
		}(&wg, city)
	}

	wg.Wait()
	log.Println("ended update forecasts")
}
