package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/martyn.andrw/weather-report/models"
)

func TestGetFromGeo(t *testing.T) {
	expected := &models.City{
		Name:    "Cheboksary",
		Lat:     56.1307195,
		Lon:     47.2449597,
		Country: "RU",
	}

	cs, err := getCityGeo(
		"http://api.openweathermap.org/geo/1.0/reverse",
		"1713d31268293b0c28017f00c0bfca66",
		56.13, 47.24,
	)

	assert.NoError(t, err)
	assert.Equal(t, 1, len(cs))
	assert.Equal(t, expected, cs[0])
}

func TestGetCity(t *testing.T) {
	// mock is required
	// service := NewCityService()
}
