package service

import "errors"

var (
	errCityNotFound = errors.New("city not found")
)
