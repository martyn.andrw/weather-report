package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/martyn.andrw/weather-report/models"
	"gitlab.com/martyn.andrw/weather-report/pkg/repository"
)

func TestUpdate(t *testing.T) {
	city := &models.City{ID: 14, Lat: 16.8909594, Lon: 100.7978225}
	pool, err := repository.NewPoolConnection(
		repository.Config{
			Username: "postgres",
			Password: "postgrespw",
			Host:     "localhost",
			Port:     "55000",
			Database: "wbcontent",
		},
	)
	assert.NoError(t, err)

	s := NewForecastService(
		repository.NewForecastRepository(pool),
		ConfigForecast{
			Apikey:      "1713d31268293b0c28017f00c0bfca66",
			WeatherLink: "http://api.openweathermap.org/data/2.5/forecast",
			Units:       "metric",
		},
	)
	err = s.UpdateForCity(city)
	assert.NoError(t, err)
}
