package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"sort"
	"time"

	"gitlab.com/martyn.andrw/weather-report/models"
)

type ForecastS struct {
	repository RepositoryForecast
	cfg        ConfigForecast
}

type ConfigForecast struct {
	WeatherLink string
	Apikey      string
	Units       string
}

type RepositoryForecast interface {
	CreateNewForecast(forecast *models.Forecast) error
	GetForecastsForCity(cityID int) ([]*models.Forecast, error)
	UpdateForecast(forecast *models.Forecast) error
	DeleteForecasts(forecastsID []int) error
	DeleteForecastsForCity(city *models.City) error
}

func NewForecastService(repositoryForecast RepositoryForecast, cfg ConfigForecast) *ForecastS {
	return &ForecastS{
		repository: repositoryForecast,
		cfg:        cfg,
	}
}

func (f *ForecastS) GetShortForecastForCity(city *models.City) (*models.ShortForecast, error) {
	forecasts, err := f.repository.GetForecastsForCity(city.ID)
	if err != nil {
		return nil, err
	}

	short := &models.ShortForecast{
		City:     *city,
		AvgTemp:  0.0,
		DateList: make([]time.Time, 0, 40),
	}

	avg := 0.0
	for _, forecast := range forecasts {
		avg += forecast.Temperature
		short.DateList = append(short.DateList, forecast.Date)
	}

	short.AvgTemp = avg / float64(len(forecasts))
	sort.Slice(short.DateList, func(i, j int) bool {
		return short.DateList[i].Before(short.DateList[j])
	})

	return short, nil
}

func (f *ForecastS) GetFullForecast(city *models.City, date time.Time) (*models.Forecast, error) {
	forecasts, err := f.repository.GetForecastsForCity(city.ID)
	if err != nil {
		return nil, err
	}

	for _, forecast := range forecasts {
		if forecast.Date == date {
			return forecast, nil
		}
	}

	return nil, errors.New("forecast not found")
}

type forecastResponce struct {
	Code string                   `json:"cod"`
	Info []map[string]interface{} `json:"list"`
}

func (f *ForecastS) UpdateForCity(city *models.City) error {
	url := fmt.Sprintf("%s?apikey=%s&lat=%v&lon=%v&units=%s", f.cfg.WeatherLink, f.cfg.Apikey, city.Lat, city.Lon, f.cfg.Units)
	res, err := http.Get(url)
	if err != nil {
		return err
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var resForecast forecastResponce
	err = json.Unmarshal(b, &resForecast)
	if err != nil {
		return err
	}

	if resForecast.Code != "200" {
		return fmt.Errorf("got a status code: %s", resForecast.Code)
	}

	err = f.repository.DeleteForecastsForCity(city)
	if err != nil {
		log.Println(err)
	}

	for _, inf := range resForecast.Info {
		tme, err := time.Parse("2006-01-02 15:04:05", inf["dt_txt"].(string))
		if err != nil {
			return err
		}

		forecast := &models.Forecast{
			CityID:        city.ID,
			Temperature:   inf["main"].(map[string]interface{})["temp"].(float64),
			Date:          tme,
			RemainingJSON: inf,
		}

		err = f.repository.CreateNewForecast(forecast)
		if err != nil {
			return err
		}
	}

	return nil
}
