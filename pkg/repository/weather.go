package repository

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/martyn.andrw/weather-report/models"
)

type ForecastRepository struct {
	pool *pgxpool.Pool
}

func NewForecastRepository(pool *pgxpool.Pool) *ForecastRepository {
	return &ForecastRepository{
		pool: pool,
	}
}

func (f *ForecastRepository) CreateNewForecast(forecast *models.Forecast) error {
	if forecast == nil {
		return errForecastIsNil
	}

	tr, err := f.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	rows, err := tr.Query(context.TODO(),
		fmt.Sprintf(
			`INSERT INTO %s ("city_ind", "dte", "daily_tmp", "rem")
			VALUES ($1, $2, $3, $4);`, forecastsTable),
		forecast.CityID, forecast.Date, forecast.Temperature, forecast.RemainingJSON,
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	rows.Close()
	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}
	return nil
}

func (f *ForecastRepository) GetForecastsForCity(cityID int) ([]*models.Forecast, error) {
	tr, err := f.pool.Begin(context.TODO())
	if err != nil {
		return nil, err
	}

	rows, err := tr.Query(context.TODO(),
		fmt.Sprintf(`SELECT * FROM %s WHERE city_ind = $1;`, forecastsTable), cityID,
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	res := make([]*models.Forecast, 0, 15)
	for rows.Next() {
		forecast := &models.Forecast{}
		err = rows.Scan(&forecast.ID, &forecast.CityID, &forecast.Date, &forecast.Temperature, &forecast.RemainingJSON)
		if err != nil {
			rows.Close()
			tr.Rollback(context.TODO())
			return nil, err
		}
		// if time.Now().Before(forecast.Date) {
		res = append(res, copyForecast(forecast))
		// }
	}

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	return res, nil
}

func (f *ForecastRepository) DeleteForecasts(forecastsID []int) error {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("DELETE FROM %s WHERE ", forecastsTable))

	for i, forecastID := range forecastsID {
		sb.WriteString(fmt.Sprintf("ind = %d", forecastID))
		if i < len(forecastsID)-1 {
			sb.WriteString(" OR ")
		}
	}
	sb.WriteRune(';')

	t, err := f.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	r, err := t.Query(context.TODO(), sb.String())
	if err != nil {
		r.Close()
		t.Rollback(context.TODO())
		return err
	}

	r.Close()
	err = t.Commit(context.TODO())
	if err != nil {
		t.Rollback(context.TODO())
		return err
	}
	return nil
}

func (f *ForecastRepository) DeleteForecastsForCity(city *models.City) error {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("DELETE FROM %s WHERE ", forecastsTable))
	sb.WriteString(fmt.Sprintf("city_ind = %d", city.ID))
	sb.WriteRune(';')

	t, err := f.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	r, err := t.Query(context.TODO(), sb.String())
	if err != nil {
		r.Close()
		t.Rollback(context.TODO())
		return err
	}

	r.Close()
	err = t.Commit(context.TODO())
	if err != nil {
		t.Rollback(context.TODO())
		return err
	}
	return nil
}

func (f *ForecastRepository) UpdateForecast(forecast *models.Forecast) error {
	tr, err := f.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	// todo rework
	rows, err := tr.Query(context.TODO(),
		fmt.Sprintf(`DO
		$do$
			BEGIN
			IF EXISTS
				(SELECT * FROM %s WHERE city_ind = $1 AND dte = $2)
				THEN
					UPDATE %s SET
						daily_tmp = $3, rem = $4
					WHERE city_ind = $1 AND dte = $2;
				ELSE
					INSERT INTO %s ("city_ind", "dte", "daily_tmp", "rem")
					VALUES ($1, $2, $3, $4);
				END IF;
			END
		$do$`, forecastsTable, forecastsTable, forecastsTable),
		forecast.CityID, forecast.Date, forecast.Temperature, forecast.RemainingJSON,
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	rows.Close()

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}
	return nil
}

func copyForecast(forecast *models.Forecast) *models.Forecast {
	return &models.Forecast{
		ID:            forecast.ID,
		CityID:        forecast.CityID,
		Temperature:   forecast.Temperature,
		Date:          forecast.Date,
		RemainingJSON: forecast.RemainingJSON,
	}
}
