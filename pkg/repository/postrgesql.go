package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	citiesTable    = "cities"
	forecastsTable = "weathers"
)

type Config struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

func NewPoolConnection(cfg Config) (*pgxpool.Pool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	log.Printf("attempting to connect to db postgres://%s:%s@%s:%s/%s\n",
		cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.Database)

	db, err := connect(ctx, fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s",
		cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.Database,
	))
	if err != nil {
		return nil, err
	}

	log.Println("attempting to ping db...",
		cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.Database)

	err = db.Ping(ctx)
	if err != nil {
		return nil, err
	}

	log.Printf("successfully connected to db postgres://%s:%s@%s:%s/%s\n",
		cfg.Username, cfg.Password, cfg.Host, cfg.Port, cfg.Database)

	return db, nil
}

func connect(ctx context.Context, connectionStr string) (conn *pgxpool.Pool, err error) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			conn, err = pgxpool.Connect(context.Background(), connectionStr)
			if err == nil {
				return
			}
		}
	}
}
