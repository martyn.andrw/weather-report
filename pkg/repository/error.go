package repository

import "errors"

var (
	errCityIsNil  = errors.New("city must be not null")
	errNameIsNil  = errors.New("city name is requred")
	errCounryCode = errors.New("country code must contains 2 uppercase letters")
	errLatBound   = errors.New("latitude must be >= -90 and <= 90")
	errLonBound   = errors.New("longitude must be >= -180 and <= 180")

	errForecastIsNil = errors.New("city must be not null")
)
