package repository

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/martyn.andrw/weather-report/models"
)

func TestCreateForecast(t *testing.T) {
	f := &models.Forecast{
		CityID:      10,
		Temperature: 22.2,
		Date:        time.Now(),
	}

	pool, _ := NewPoolConnection(Config{
		Username: "postgres",
		Password: "postgrespw",
		Host:     "localhost",
		Port:     "55000",
		Database: "wbcontent",
	})
	r := NewForecastRepository(pool)
	r.CreateNewForecast(f)
}

func TestGetForecastsForCity(t *testing.T) {
	pool, _ := NewPoolConnection(Config{
		Username: "postgres",
		Password: "postgrespw",
		Host:     "localhost",
		Port:     "55000",
		Database: "wbcontent",
	})
	r := NewForecastRepository(pool)

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      10,
	// 	Temperature: 22.2,
	// 	Date:        time.Now(),
	// })

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      10,
	// 	Temperature: 23,
	// 	Date:        time.Now().Add(2 * time.Hour),
	// })

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      11,
	// 	Temperature: 23.2,
	// 	Date:        time.Now().Add(2 * time.Hour),
	// })

	res, err := r.GetForecastsForCity(14)
	assert.NoError(t, err)
	assert.NotEqual(t, 0, len(res))

	for _, r := range res {
		t.Log(r)
	}
}

func TestDeleteForecasts(t *testing.T) {
	pool, _ := NewPoolConnection(Config{
		Username: "postgres",
		Password: "postgrespw",
		Host:     "localhost",
		Port:     "55000",
		Database: "wbcontent",
	})
	r := NewForecastRepository(pool)

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      10,
	// 	Temperature: 22.2,
	// 	Date:        time.Now(),
	// })

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      10,
	// 	Temperature: 23,
	// 	Date:        time.Now().Add(2 * time.Hour),
	// })

	// r.CreateNewForecast(&models.Forecast{
	// 	CityID:      11,
	// 	Temperature: 23.2,
	// 	Date:        time.Now().Add(2 * time.Hour),
	// })

	err := r.DeleteForecasts([]int{35, 36, 37})
	assert.NoError(t, err)
}
