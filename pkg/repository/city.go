package repository

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/martyn.andrw/weather-report/models"
)

type CityRepository struct {
	pool *pgxpool.Pool
}

func NewCityRepository(pool *pgxpool.Pool) *CityRepository {
	return &CityRepository{
		pool: pool,
	}
}

func (c *CityRepository) AddNewCity(city *models.City) error {
	if city == nil {
		return errCityIsNil
	}
	if city.Name == "" {
		return errNameIsNil
	}
	if city.Country == "" || len(city.Country) != 2 {
		return errCounryCode
	}

	if city.Lat < -90 || city.Lat > 90 {
		return errLatBound
	}
	if city.Lon < -180 || city.Lon > 180 {
		return errLonBound
	}

	tr, err := c.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	rows := tr.QueryRow(context.TODO(),
		fmt.Sprintf(
			`INSERT INTO %s ("nam", "country", "lat", "lon")
			VALUES ($1, $2, $3, $4);`, citiesTable),
		city.Name, strings.ToUpper(city.Country), city.Lat, city.Lon,
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	err = rows.Scan(&city.ID)
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}
	return nil
}

func (c *CityRepository) AddNewCities(cities []*models.City) error {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf(`INSERT INTO %s ("nam", "country", "lat", "lon")	VALUES `, citiesTable))

	for i, city := range cities {
		if city == nil {
			return errCityIsNil
		}
		if city.Name == "" {
			return errNameIsNil
		}
		if city.Country == "" || len(city.Country) != 2 {
			return errCounryCode
		}

		if city.Lat < -90 || city.Lat > 90 {
			return errLatBound
		}
		if city.Lon < -180 || city.Lon > 180 {
			return errLonBound
		}

		sb.WriteString(
			fmt.Sprintf(" ('%s', '%s', %v, %v)",
				city.Name, city.Country, city.Lat, city.Lon,
			))

		if i < len(cities)-1 {
			sb.WriteRune(',')
		}
	}

	sb.WriteString(" RETURNING ind;")
	tr, err := c.pool.Begin(context.TODO())
	if err != nil {
		return err
	}

	rows, err := tr.Query(
		context.TODO(),
		sb.String(),
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	for i := 0; rows.Next() && i < len(cities); i++ {
		err = rows.Scan(&cities[i].ID)
		if err != nil {
			tr.Rollback(context.TODO())
			return err
		}
	}

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return err
	}

	return nil
}

func (c *CityRepository) GetCityByName(name string) (*models.City, error) {
	tr, err := c.pool.Begin(context.TODO())
	if err != nil {
		return nil, err
	}

	city := &models.City{}

	row := tr.QueryRow(context.TODO(),
		fmt.Sprintf(
			`SELECT * FROM %s
			WHERE nam LIKE $1;`,
			citiesTable),
		name,
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	err = row.Scan(&city.ID, &city.Name, &city.Country, &city.Lat, &city.Lon)
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	return city, nil
}

func (c *CityRepository) GetCities() ([]*models.City, error) {
	tr, err := c.pool.Begin(context.TODO())
	if err != nil {
		return nil, err
	}

	rows, err := tr.Query(context.TODO(),
		fmt.Sprintf(`SELECT * FROM %s;`, citiesTable),
	)
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	res := make([]*models.City, 0, 20)
	for rows.Next() {
		city := &models.City{}
		err = rows.Scan(&city.ID, &city.Name, &city.Country, &city.Lat, &city.Lon)
		if err != nil {
			rows.Close()
			tr.Rollback(context.TODO())
			return nil, err
		}
		res = append(res, copyCity(city))
	}

	err = tr.Commit(context.TODO())
	if err != nil {
		tr.Rollback(context.TODO())
		return nil, err
	}

	return res, nil
}

func copyCity(city *models.City) *models.City {
	return &models.City{
		ID:      city.ID,
		Name:    city.Name,
		Country: city.Country,
		Lat:     city.Lat,
		Lon:     city.Lon,
	}
}
