package repository

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/martyn.andrw/weather-report/models"
)

func TestAdd(t *testing.T) {
	conn, err := connect(context.Background(), "postgres://postgres:postgrespw@localhost:55000/wbcontent")
	assert.NoError(t, err)

	r := NewCityRepository(conn)
	err = r.AddNewCity(&models.City{
		Name:    "Test",
		Country: "TC",
		Lat:     0.1,
		Lon:     0.2,
	})
	assert.NoError(t, err)
}

func TestAddCities(t *testing.T) {
	conn, err := connect(context.Background(), "postgres://postgres:postgrespw@localhost:55000/wbcontent")
	assert.NoError(t, err)

	r := NewCityRepository(conn)
	err = r.AddNewCities([]*models.City{
		{
			Name:    "Test0",
			Country: "TC",
			Lat:     0.1,
			Lon:     0.2,
		},
		{
			Name:    "Test1",
			Country: "TC",
			Lat:     0.12,
			Lon:     0.23,
		},
		{
			Name:    "Test3",
			Country: "TC",
			Lat:     0.12,
			Lon:     0.23,
		},
	})
	assert.NoError(t, err)
}

func TestAddNeg(t *testing.T) {
	conn, err := connect(context.Background(), "postgres://postgres:postgrespw@localhost:55000/wbcontent")
	assert.NoError(t, err)

	r := NewCityRepository(conn)
	err = r.AddNewCity(&models.City{
		Name: "Test",
		Lat:  0.1,
		Lon:  0.2,
	})
	assert.ErrorIs(t, err, errCounryCode)
}

func TestGet(t *testing.T) {
	conn, err := connect(context.Background(), "postgres://postgres:postgrespw@localhost:55000/wbcontent")
	assert.NoError(t, err)

	expect := &models.City{
		Name:    "Test1",
		Country: "TC",
		Lat:     0.1,
		Lon:     0.2,
	}

	r := NewCityRepository(conn)
	err = r.AddNewCity(expect)
	assert.NoError(t, err)

	city, err := r.GetCityByName(expect.Name)
	assert.NoError(t, err)
	assert.Equal(t, expect, city)
}
