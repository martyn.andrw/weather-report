package handler

import (
	"encoding/json"
	"net/http"
)

func (h *Handler) GetCities(w http.ResponseWriter, r *http.Request) {
	cities := h.services.GetOrderedCities()
	b, err := json.Marshal(cities)
	if err != nil {
		sendError(w, err, http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.Write(b)
}
