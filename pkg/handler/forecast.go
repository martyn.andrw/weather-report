package handler

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/martyn.andrw/weather-report/models"
)

type shortForecastRequest struct {
	CityName string `json:"city"`
}

func (h *Handler) GetShortForecast(w http.ResponseWriter, r *http.Request) {
	sfr := shortForecastRequest{}
	b, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}
	defer r.Body.Close()

	err = json.Unmarshal(b, &sfr)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	c, err := h.services.GetCityByName(sfr.CityName)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	f, err := h.services.GetShortForecastForCity(c)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	if len(f.DateList) == 0 {
		err = sendError(w, errors.New("city has no forecasts"), http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	body, err := json.Marshal(f)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.Write(body)
}

type fullForecastRequest struct {
	CityName string    `json:"city"`
	Time     time.Time `json:"date"`
}

type fullForecastResponce struct {
	City     models.City `json:"city"`
	Forecast forecast    `json:"forecast"`
}

type forecast struct {
	Temp float64                `json:"temperature"`
	Date time.Time              `json:"date"`
	Rem  map[string]interface{} `json:"additional information"`
}

func (h *Handler) GetForecast(w http.ResponseWriter, r *http.Request) {
	ffr := fullForecastRequest{}
	b, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}
	defer r.Body.Close()

	err = json.Unmarshal(b, &ffr)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	c, err := h.services.GetCityByName(ffr.CityName)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	f, err := h.services.GetFullForecast(c, ffr.Time)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	ffresp := fullForecastResponce{
		City: *c,
		Forecast: forecast{
			Temp: f.Temperature,
			Date: f.Date,
			Rem:  f.RemainingJSON,
		},
	}

	body, err := json.Marshal(ffresp)
	if err != nil {
		log.Println(err)

		err = sendError(w, err, http.StatusBadRequest)
		if err != nil {
			log.Println(err)
		}
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.Write(body)
}
