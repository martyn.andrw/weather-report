package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/martyn.andrw/weather-report/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{
		services: services,
	}
}

type errorResponce struct {
	Error string `json:"error"`
}

func sendError(w http.ResponseWriter, err error, status int) error {
	errorR := errorResponce{Error: err.Error()}

	b, err := json.Marshal(errorR)
	if err != nil {
		return err
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(status)
	_, err = w.Write(b)
	return err
}
