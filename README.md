# Weather report

/get_cities - для просмотра отсортированного списка городов

Пример запроса

```json
{
}
```

/get_short_forecast - для просмотра короткой сводки по городу

Пример запроса

```json
{
    "city" : "Ban Yaeng"
}
```

/get_full_forecast - просмотр полного предсказания в городе в определенную дату

Пример запроса

```json
{
    "city" : "Ban Yaeng",
    "date" : "2022-10-27T00:00:00Z"
}
```
