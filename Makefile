run:
	ls go.mod || go mod init gitlab.com/martyn.andrw/weather-report
	go mod tidy
	go mod download
	go run cmd/main.go

build-run:
	ls go.mod || go mod init gitlab.com/martyn.andrw/weather-report
	go mod tidy
	go mod download
	go build -o weather-report cmd/main.go
	./weather-report