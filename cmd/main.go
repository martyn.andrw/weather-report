package main

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/martyn.andrw/weather-report/pkg/handler"
	"gitlab.com/martyn.andrw/weather-report/pkg/repository"
	"gitlab.com/martyn.andrw/weather-report/pkg/service"
	"gitlab.com/martyn.andrw/weather-report/server"
)

func main() {
	err := initConfig()
	checkFatalErr(err)

	pool, err := repository.NewPoolConnection(repository.Config{
		Username: viper.GetStringMapString("db")["username"],
		Password: viper.GetStringMapString("db")["password"],
		Host:     viper.GetStringMapString("db")["host"],
		Port:     viper.GetStringMapString("db")["port"],
		Database: viper.GetStringMapString("db")["database"],
	})
	checkFatalErr(err)
	defer pool.Close()

	cityRepository := repository.NewCityRepository(pool)
	cityService, err := service.NewCityService(cityRepository, service.ConfigCity{
		GeoReverseLink: viper.GetStringMapString("api")["geo_reverse_link"],
		GeoDirectLink:  viper.GetStringMapString("api")["geo_direct_link"],
		Apikey:         viper.GetStringMapString("api")["key"],
	})
	checkFatalErr(err)

	forecastRepository := repository.NewForecastRepository(pool)
	forecastService := service.NewForecastService(forecastRepository, service.ConfigForecast{
		WeatherLink: viper.GetStringMapString("api")["forecast_link"],
		Apikey:      viper.GetStringMapString("api")["key"],
		Units:       viper.GetStringMapString("api")["units"],
	})

	services := service.NewService(cityService, forecastService)
	handlers := handler.NewHandler(services)

	go services.UpdateEveryMinute()

	serv := server.NewServer()
	serv.HandleFunc("/get_cities", handlers.GetCities)
	serv.HandleFunc("/get_short_forecast", handlers.GetShortForecast)
	serv.HandleFunc("/get_full_forecast", handlers.GetForecast)

	err = serv.Run(
		viper.GetStringMapString("app")["host"],
		viper.GetStringMapString("app")["port"],
	)
	checkFatalErr(err)
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func checkFatalErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
